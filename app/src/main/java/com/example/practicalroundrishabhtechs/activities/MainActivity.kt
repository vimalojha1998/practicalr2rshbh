package com.example.practicalroundrishabhtechs.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.practicalroundrishabhtechs.R
import com.example.practicalroundrishabhtechs.adapters.RecyclerviewAdapter
import com.example.practicalroundrishabhtechs.data.DataViewModel
import com.example.practicalroundrishabhtechs.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var mViewModel: DataViewModel
    lateinit var mAdapter: RecyclerviewAdapter
    private val listCount = 30
    private lateinit var mBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        init()
    }

    private fun init() {
        initRecyclerview()
        mViewModel = ViewModelProvider(this)[DataViewModel::class.java]

        mViewModel.mCheckboxListData.observe(this, Observer {
            if (it == null) return@Observer
            mAdapter.setList(it)
        })

        mViewModel.createCheckboxList(listCount)
    }

    private fun initRecyclerview() {
        mAdapter = RecyclerviewAdapter(this)
        mBinding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.VERTICAL, false)
            adapter = mAdapter
        }

    }
}