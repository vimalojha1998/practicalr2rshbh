package com.example.practicalroundrishabhtechs.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.practicalroundrishabhtechs.R
import com.example.practicalroundrishabhtechs.data.CheckBoxModel
import com.example.practicalroundrishabhtechs.databinding.ItemRecyclerviewRowBinding

class RecyclerviewAdapter(val context: Context) :
    RecyclerView.Adapter<RecyclerviewAdapter.ViewHolder>() {

    private var mList: List<CheckBoxModel> = ArrayList()
    private val mHash = HashMap<Int, CheckBoxModel>()

    inner class ViewHolder(binding: ItemRecyclerviewRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val mBinding: ItemRecyclerviewRowBinding = binding

        fun bind(item: CheckBoxModel, index: Int) {

            val linearLayout = LinearLayout(context)
            linearLayout.orientation = LinearLayout.HORIZONTAL
            linearLayout.removeAllViews()
            if (mHash.containsKey(index))
                item.hashMap = mHash[index]?.hashMap
            else
                item.hashMap = HashMap()

            for (i in 0 until item.checkBoxCount) {
                val checkBox = CheckBox(context)
                checkBox.id = i
                if (item.hashMap != null && item.hashMap!!.containsKey(i)) {
                    checkBox.isChecked = item.hashMap!![i]!!
                } else {
                    item.hashMap!![i] = false
                }
                checkBox.setOnCheckedChangeListener { compoundButton, b ->
                    if (item.hashMap != null)
                        item.hashMap!![checkBox.id] = b
                }
                linearLayout.addView(checkBox)
            }

            mHash[index] = item
            mBinding.horizontalView.removeAllViews()
            mBinding.horizontalView.addView(linearLayout)
            mBinding.textview.text = item.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mBinding: ItemRecyclerviewRowBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.item_recyclerview_row,
            parent,
            false
        )
        return ViewHolder(mBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mList[position], position)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    fun setList(list: List<CheckBoxModel>) {
        mList = list
        notifyDataSetChanged()
    }


}