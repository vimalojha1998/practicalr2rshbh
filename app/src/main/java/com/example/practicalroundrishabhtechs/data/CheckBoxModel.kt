package com.example.practicalroundrishabhtechs.data

data class CheckBoxModel(val title: String, val checkBoxCount: Int, var hashMap: HashMap<Int, Boolean>?)
