package com.example.practicalroundrishabhtechs.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DataViewModel : ViewModel() {

    var checkBoxList = MutableLiveData<List<CheckBoxModel>>()
    val mCheckboxListData: LiveData<List<CheckBoxModel>> = checkBoxList

    fun createCheckboxList(listCount: Int) {
        var tempList = mutableListOf<CheckBoxModel>()

        for (i in 0 until listCount) {
            val hash = HashMap<Int, Boolean>()
            hash[i] = false
            val checkBox = CheckBoxModel("Title $i", i+1, null)
            tempList.add(checkBox)
        }
        checkBoxList.value = tempList
    }
}